using buddy.Should;
using StackExtend;
import Shuffle.*;
import haxe.ds.GenericStack;
import Factor.stack;

class ShuffleTests extends buddy.BuddySuite
{
public function new() {
	var s:GenericStack<Dynamic> = stack;
	describe("vocab: shuffle",{
		beforeEach(function () {
			s.clear();// = StackExtend.make();
		});
		
		it("dup", {
			s.push(1);
			_dup();
			s.popn(2).should.containExactly([1,1]);
		});
			it("drop", {
				// before:
				s.push('a');
				// action:
				_drop();
				// expected after: should be
				s.isEmpty().should.be(true);
		});
		
	
});

 }
}