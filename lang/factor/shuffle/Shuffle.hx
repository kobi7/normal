import Factor.stack as s;
import Factor.retain as r;
import Factor.*;
using StackExtend;
import StackExtend.*;
/*

	// s.call(2,1,add);
	// initial idea was:
	static function add(args:Two) : One {
		return args1(args.a+args.b);
	}
	// but i reverted to using arrays as input and output arguments.
*/


// built in shuffle words + complex + shuffle vocab.
@:allow(ShuffleTests)
class Shuffle {
	var s = Factor.stack; 
	var r = Factor.retain;
	
	static function rTo() {
		s.push(r.pop());
	}
	static function toR() {
		r.push(s.pop());
	}
	
	static function get(n:UInt):Array<Dynamic> {
		var arr = [];
		for (i in 0...n)
		{
			arr.push(s.pop());
		}
		return arr;
	}
	
	// Removing stack elements:
	
	//  drop ( x -- )
	static function _drop() {
		s.pop();
	}

	// 2drop ( x y -- )
	static function _2drop() {
		_drop(); _drop();
		
	}

	// 3drop ( x y z -- )
	static function _3drop() {
		_2drop(); _drop();
		
	}
	// nip ( x y -- y )
	static function _nip()
	{
		toR();
		_drop();
		rTo();
		
	}
	
	// 2nip ( x y z -- z )
	static function _2nip() {
		_nip(); _nip();
	}
	
	// Duplicating stack elements:

	// dup ( x -- x x )
	static function _dup() {
		var a = s.pop();
		s.push(a);
		s.push(a);
	}


	// 2dup ( x y -- x y x y )
	static function _2dup() {
		var items = s.popn(2);
		s.pushn(items);
		s.pushn(items);
	}

	// 3dup ( x y z -- x y z x y z )
	static function _3dup() {
		// take 3
		var items = s.popn(3);
		// return 3
		s.pushn(items);
		// again:
		s.pushn(items);
	}

	// over ( x y -- x y x )
	static function _over() {
		_2dup(); // x y x y
		_drop();
	}

	// 2over ( x y z -- x y z x y )
	static function _2over() {
		_3dup(); _drop();
	}

	// pick ( x y z -- x y z x )
	static function _pick() {
		_2over(); // x y z x y 
		_drop();  // x y z x
	}

	// Permuting stack elements:
	// swap ( x y -- y x )
	static function _swap() {
		var y = s.pop();
		var x = s.pop();
		s.push(y);
		s.push(x);
	}
	
	// Duplicating stack elements deep in the stack:
	// dupd ( x y -- x x y )
	static function _dupd() {
		toR();
		_dup();
		rTo();	
	}

	// Permuting stack elements deep in the stack:
	// swapd ( x y z -- y x z )
	static function _swapd() {
		toR();
		_swap();
		rTo();
	}

	// rot ( x y z -- y z x )
	static function _rot() {
		var yz = s.popn(2);
		toR();
		s.pushn(yz);
		rTo();
	}

	// -rot ( x y z -- z x y )
	static function _invRot() {
		var z = s.pop();
		var y = s.pop();
		var x = s.pop();
		s.pushn([z,x,y]);
	}



	
}