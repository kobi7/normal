using buddy.Should;



class Tests extends buddy.BuddySuite
{
	public static function main() {
		var stack = new StackTests();
		var shuffle = new ShuffleTests();
		
		var runner = new buddy.SuitesRunner(
		[stack, shuffle]
		);
		runner.run();
	}
	
}