import haxe.ds.GenericStack;

class Factor {
	public static var stack(default,never) = new GenericStack<Dynamic>();	
	public static var retain(default,never) = new GenericStack<Dynamic>();	
	public static function fcall (args :Array<Dynamic> , fn:Array<Dynamic> -> Array<Dynamic> ) : Array<Dynamic>
	{
		var result:Array<Dynamic>;
		result = fn(args);	
		return result;
	}
}