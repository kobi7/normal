using buddy.Should;
using StackExtend;
using Slambda;
import haxe.ds.GenericStack;

// typedef Stack = GenericStack<Dynamic>;


class StackTests extends buddy.BuddySuite
{
	public function new() {
		var s = new GenericStack<Dynamic>();
			describe("Stack is now able to: hold items!",{
				
				beforeEach({s.clear();});
				
				it("wants to: add one item", {
				//doing it
				s.push(1);
				// and it did!
				// as evidence we show:
				s.pop().should.be(1);
				});
				
				it("wants to: add a few items, and retrieve in correct order", {
				//doing it
				s.pushn([1,2,3]);
				// and it did!
				// as evidence we show:
				var got = s.popn(3);
				got.should.containExactly([1,2,3]);
				});
				
				it("wants to: add many items, and retrieve all.", {
				//doing it
				s.push(1);
				s.push(2);
				s.push(3);
				s.push(4);
				s.push(5);
				s.push(6);
				s.push(7);
				// and it did!
				// as evidence we show:
				var got = s.popn(7);
				got.should.containExactly([1,2,3,4,5,6,7]);
				});

				it("wants to: get null if stack is empty", {
					//doing it
					s.clear();
					// and it did!
					// as evidence we show:
					var got = s.pop();
					got.should.be(null);
					
				});
				
				it("wants to: reversed order with many, but right order with ones", {
					s.clear();
					s.push(1);
					s.pop().should.be(1); 
					s.push(2);
					s.pop().should.be(2);
					s.push(3); s.push(4);
					var got = s.popn(2);
					got.should.containExactly([3,4]);
		
				});

			});
	}
}