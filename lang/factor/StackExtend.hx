import Sure.sure as assert;
import Sure.sure as require;
import haxe.ds.GenericStack;
typedef Stack = GenericStack<Dynamic>;

@:allow(StackTests)
class StackExtend {
	// pop+push to stack.
	
	public static function make():Stack
	{
		return new Stack();
	}
	public static function popn(s:Stack,count:UInt):Array<Dynamic> 
	{
		var res = [];
		for (_ in 0...count)
		{
			assert (!s.isEmpty()); // MUST FAIL if assumptions are wrong. (early fail)
			var x = s.pop();
			res.push(x);
		}
		res.reverse();
		return res;
	}
	
	// @:allow(StackTests)
	public static function pushn(s:Stack, args:Array<Dynamic>) {
		for (item in args)
		{
			s.add(item);
		}
	}
	
	public static function clear(s:Stack) {
		s = new Stack();	
	}
	public static function push (s:Stack, arg:Dynamic) : Void 
	{	
		//$type(s);
		s.add(arg);
	}
	/*static function array2args(arr:Array<Dynamic>) : Dynamic
	{
		require (arr.length <=5 );
		var result;
		switch(arr.length)
		{
			case 0:
				result = {count:0};
			case 1:
				result = {count:1, a:arr[0]};
			case 2:
				result = {count:2, a:arr[0], b:arr[1]};
			case 3:
				result = {count:3, a:arr[0], b:arr[1], c:arr[2]};
			case 4:
				result = {count:4, a:arr[0], b:arr[1], c:arr[2], d:arr[3]};
			case 5:
				result = {count:5, a:arr[0], b:arr[1], c:arr[2], d:arr[3], e:arr[4]};
				
			default:
				throw 'unimplemented for $arr.length';
		}
		return result;
	}*/
	
	/*
	public static function args1(arg) {
		return array2args([arg]);
	}	
	public static function args2(arg1, arg2) {
		return array2args([arg1, arg2]);
	}	
	public static function args3(arg1, arg2, arg3) {
		return array2args([arg1, arg2, arg3]);
	}
	public static function args4(arg1, arg2, arg3, arg4) {
		return array2args([arg1, arg2, arg3, arg4]);
	}
	public static function args5(arg1, arg2, arg3, arg4, arg5) {
		return array2args([arg1, arg2, arg3, arg4, arg5]);
	}*/
	
	/*
	static function args2array(out_count:UInt,args:Dynamic) :Array<Dynamic>
	{
		var res=[];
		switch(out_count)
		{
			case 5:
				res = [args.a, args.b, args.c, args.d, args.e];
			case 4:
				res = [args.a, args.b, args.c, args.d ];
			case 3:
				res = [args.a, args.b, args.c];
			case 2:
				res = [args.a, args.b];
			case 1:
				res = [args.a];
				
			default:
				assert(false);
		}
		return res;
	}
	*/
	public static function call (s:Stack, in_count:UInt, out_count:UInt, fn:Array<Dynamic> -> Array<Dynamic>)
	{
		var items = popn(s,in_count);
		// var args = array2args(items);
		if (out_count == 0)
			fn(items);
		else
		{
			var res = fn(items);	
			assert (out_count == res.length);
			var result_items = res;
			pushn(s,result_items);
		}
	}
	
}